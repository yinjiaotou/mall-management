import { useCookies } from '@vueuse/integrations/useCookies'

const tokenKey = 'admin-token'
const cookie = useCookies()

export const getToken = () => cookie.get(tokenKey)
export const setToken = (tooken) => cookie.set(tokenKey, tooken)
export const removeToken = () => cookie.remove(tokenKey)

export const getTabList = () => cookie.get('tabList')
export const setTabList = (tabList) => cookie.set('tabList', tabList)

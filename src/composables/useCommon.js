import { ref, reactive, onMounted, computed } from 'vue'
import { toast } from '@/composables/util'

// 列表
export const useInitTable = (opt = {}) => {
  // 重置搜索
  let searchForm = null
  let resetSearchForm = null
  if (opt.searchForm) {
    searchForm = reactive({ ...opt.searchForm })

    resetSearchForm = () => {
      for (const key in opt.searchForm) {
        searchForm[key] = opt.searchForm[key]
      }
      getData()
    }
  }
  const tableData = ref([])
  const loading = ref(false)

  // 分页
  const currentPage = ref(1)
  const total = ref(0)
  const limit = ref(10)

  // 获取数据
  const getData = (p = null) => {
    if (typeof p == 'number') {
      currentPage.value = p
    }
    loading.value = true
    opt
      .getData(currentPage.value, searchForm)
      .then((res) => {
        tableData.value = res.data.list

        total.value = res.data.totalCount
        if (opt.onGetListSuccess && typeof opt.onGetListSuccess == 'function')
          opt.onGetListSuccess(res)
      })
      .finally(() => {
        loading.value = false
      })
  }

  // 修改状态
  const updateStatus = (status, id) => {
    opt.updateStatus(id, status).then((res) => {
      toast('操作成功！')
      getData(currentPage.value)
    })
  }

  // 多选 删除
  const multipleSelection = ref([])
  const multipleSelectionRef = ref(null)
  const handleSelectionChange = (val) => {
    multipleSelection.value = val.map((item) => item.id)
  }

  const handeMultiDelete = () => {
    loading.value = true
    opt
      .delete(multipleSelection.value)
      .then((res) => {
        if (multipleSelectionRef.value)
          multipleSelectionRef.value.clearSelection()
        toast('删除成功！')
        getData(currentPage.value)
      })
      .finally(() => (loading.value = false))
  }

  onMounted(() => {
    getData()
  })

  return {
    searchForm,
    resetSearchForm,
    tableData,
    loading,
    currentPage,
    total,
    limit: 10,
    getData,
    multipleSelectionRef,
    handeMultiDelete,
    handleSelectionChange,
    updateStatus,
  }
}

// 新增
export const useInitForm = (opt = {}) => {
  // 表单部分
  const formDrawerRef = ref(null)
  const formRef = ref(null)
  const form = reactive({})
  const editId = ref(0)
  const defaultForm = opt.form
  const rules = opt.rules || {}
  const drawerTitle = computed(() => (editId.value ? '修改' : '新增'))

  // 提交
  const handleSubmit = () => {
    formRef.value.validate((valid) => {
      if (!valid) return

      formDrawerRef.value.showLoading(true)

      const fun = editId.value
        ? opt.update(editId.value, form)
        : opt.create(form)

      fun
        .then((res) => {
          toast(drawerTitle.value + '成功')
          // 修改刷新当前页，新增刷新第一页
          opt.getData(editId.value ? false : 1)
          formDrawerRef.value.showDrawer(false)
        })
        .finally(() => {
          formDrawerRef.value.showLoading(false)
        })
    })
  }

  function resetForm(row = false) {
    if (formRef.value) formRef.value.clearValidate()
    if (row) {
      for (const key in defaultForm) {
        form[key] = row[key]
      }
    }
  }
  // 新增
  const handleCreate = () => {
    editId.value = 0
    resetForm(defaultForm)
    formDrawerRef.value.showDrawer(true)
  }

  // 编辑
  const handleEdit = (row) => {
    editId.value = row.id
    resetForm(row)
    formDrawerRef.value.showDrawer(true)
  }
  // 删除
  const handleDelete = (id) => {
    opt.delete(id).then((res) => {
      toast('删除成功')
      opt.getData()
    })
  }
  return {
    formDrawerRef,
    formRef,
    form,
    editId,
    rules,
    drawerTitle,
    handleSubmit,
    handleCreate,
    handleEdit,
    handleDelete,
  }
}

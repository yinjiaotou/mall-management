import axios from '@/axios'

//图库管理
export const getImageClass = (data) =>
  axios.get(`/admin/image_class/${data.page}`)

export const addImageClass = (data) => axios.post(`/admin/image_class`, data)
export function getImageList(id, page = 1) {
  return axios.get(`/admin/image_class/${id}/image/${page}`)
}

export function updateImageClass(id, data) {
  return axios.post('/admin/image_class/' + id, data)
}

export function deleteImageClass(id) {
  return axios.post(`/admin/image_class/${id}/delete`)
}

export function updateImage(id, name) {
  return axios.post(`/admin/image/${id}`, { name })
}

export function deleteImage(ids) {
  return axios.post(`/admin/image/delete_all`, { ids })
}
export const uploadImageAction = '/api/admin/image/upload'

// 公告列表
export const getNoticeList = (page) => axios.get(`/admin/notice/${page}`)

export const addNotice = (data) => axios.post(`/admin/notice`, data)

export const updateNotice = (id, data) =>
  axios.post(`/admin/notice/${id}`, data)

export const deleteNotice = (id) => axios.post(`admin/notice/${id}/delete`)

// 商品规格
export const getSkus = (page) => axios.get(`/admin/skus/${page}`)

export const addSkus = (data) => axios.post(`/admin/skus`, data)

export const updateSkus = (id, data) => axios.post(`/admin/skus/${id}`, data)

export const deleteAllSkus = (ids) =>
  axios.post(`admin/skus/delete_all`, { ids })

export const updateSkusStatus = (id, status) =>
  axios.post(`/admin/skus/${id}/update_status`, {
    status,
  })

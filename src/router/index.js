import { createRouter, createWebHashHistory } from 'vue-router'

import Index from '@/pages/index.vue'
import NotFound from '@/pages/404.vue'
import Login from '@/pages/login.vue'
import Admin from '@/layout/index.vue'
import GoodList from '@/views/goods/list.vue'
import Category from '@/views/category/list.vue'
import UserList from '@/views/user/list.vue'
import OrderList from '@/views/order/list.vue'
import CommentList from '@/views/comment/list.vue'
import ImageList from '@/views/image/list.vue'
import NoticeList from '@/views/notice/list.vue'
import SettingBase from '@/views/setting/base.vue'
import CouponList from '@/views/coupon/list.vue'

import Manager from '@/views/manager/list.vue'
import Skus from '@/views/skus/list.vue'

const routes = [
  {
    path: '/',
    name: 'admin',
    component: Admin,
  },
  {
    path: '/login',
    component: Login,
    meta: {
      title: '登录页',
    },
  },
  {
    path: '/:pathMatch(.*)*',
    component: NotFound,
  },
]

const asyncRoutes = [
  {
    path: '/',
    name: '/',
    component: Index,
    meta: {
      title: '后台首页',
    },
  },
  {
    path: '/goods/list',
    name: '/goods/list',
    component: GoodList,
    meta: {
      title: '商品管理',
    },
  },
  {
    path: '/user/list',
    name: '/user/list',
    component: UserList,
    meta: {
      title: '用户列表',
    },
  },
  {
    path: '/category/list',
    name: '/category/list',
    component: Category,
    meta: {
      title: '分类列表',
    },
  },

  {
    path: '/order/list',
    name: '/order/list',
    component: OrderList,
    meta: {
      title: '订单列表',
    },
  },
  {
    path: '/comment/list',
    name: '/comment/list',
    component: CommentList,
    meta: {
      title: '评价列表',
    },
  },
  {
    path: '/image/list',
    name: '/image/list',
    component: ImageList,
    meta: {
      title: '图库列表',
    },
  },
  {
    path: '/notice/list',
    name: '/notice/list',
    component: NoticeList,
    meta: {
      title: '公告列表',
    },
  },
  {
    path: '/setting/base',
    name: '/setting/base',
    component: SettingBase,
    meta: {
      title: '配置',
    },
  },
  {
    path: '/coupon/list',
    name: '/coupon/list',
    component: CouponList,
    meta: {
      title: '优惠券列表',
    },
  },
  {
    path: '/manager/list',
    name: '/manager/list',
    component: Manager,
    meta: {
      title: '管理员',
    },
  },
  {
    path: '/skus/list',
    name: '/skus/list',
    component: Skus,
    meta: {
      title: '规格管理',
    },
  },
]
export const router = createRouter({
  history: createWebHashHistory(),
  routes,
})

export const addRouters = (menus) => {
  let hasNewRoutes = false
  const findAndAddRoutesBymenus = (arr) => {
    arr.forEach((e) => {
      let item = asyncRoutes.find((o) => o.path == e.frontpath)
      if (item && !router.hasRoute(item.path)) {
        router.addRoute('admin', item)
        hasNewRoutes = true
      }
      if (e.child && e.child.length > 0) {
        findAndAddRoutesBymenus(e.child)
      }
    })
  }
  findAndAddRoutesBymenus(menus)

  return hasNewRoutes
}
